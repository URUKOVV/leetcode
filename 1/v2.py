from typing import List
# O(2n)


class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        val_key = dict((val, index) for index, val in enumerate(nums))
        for key, value in enumerate(nums):
            sec_key = val_key.get(target - value)
            if sec_key != None and sec_key != key:
                return [key, sec_key]
        return []
