# dumb solution, could be better, faster than 91.18% but memory less than 13.93%
# TODO without simulation

class Solution:
    def convert(self, s: str, numRows: int) -> str:
        if numRows == 1:
            return s

        row_offset = 1
        row_counter = 0
        arr = ["" for _ in range(numRows)]

        for character in s:
            arr[row_counter] += character
            row_counter += row_offset
            if row_counter == numRows or row_counter == -1:
                row_offset = row_offset * -1
                row_counter += row_offset + row_offset
        return "".join(arr)


print(Solution().convert("PAYPALISHIRING", 3))
"PAHNAPLSIIGYIR"