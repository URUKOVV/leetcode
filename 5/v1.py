# O(n^2)

class Solution:
    def longestPalindrome(self, s: str) -> str:
        max_length = 0
        max_start = 0

        for i in range(len(s)):
            # цикл для обработки двух случаев (четная и нечетеная длина палиндрома)
            for start, end in ((i, i), (i, i + 1)):
                while start >= 0 and end < len(s) and s[start] == s[end]:
                    start -= 1
                    end += 1
                if end - start - 1 > max_length:
                    max_length = end - start - 1
                    max_start = start + 1

        return s[max_start:max_start+max_length]
