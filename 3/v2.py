# O(n)

class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        passed = {}
        maximum = 0
        end = 0
        start = 0
        while end < len(s):
            char = s[end]

            if char in passed:
                start = max(start, passed[char] + 1)
            passed[char] = end
            maximum = max(maximum, end - start + 1)
            end += 1

        return maximum

Solution().lengthOfLongestSubstring("abba")