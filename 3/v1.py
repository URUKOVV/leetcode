class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        passed = {}
        maximum = 0
        counter = 0
        i = 0
        while i < len(s):
            char = s[i]

            if char in passed:
                i = passed[char] + 1
                if counter > maximum:
                    maximum = counter
                passed.clear()
                counter = 0
            else:
                counter += 1
                passed[char] = i
                i += 1
                if counter > maximum:
                    maximum = counter

        return maximum

Solution().lengthOfLongestSubstring("abcabcbb")