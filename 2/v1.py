from typing import Optional
# O(n)


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def addTwoNumbers(self, l1: Optional[ListNode], l2: Optional[ListNode]) -> Optional[ListNode]:
        first_int = 0
        second_int = 0

        node = l1
        counter = 0
        while node != None:
            first_int += int(node.val * (10 ** counter))
            node = node.next
            counter += 1

        node = l2
        counter = 0
        while node != None:
            second_int += int(node.val * (10 ** counter))
            node = node.next
            counter += 1

        third_int = first_int + second_int

        first_node = ListNode(third_int % 10)
        prev_node = first_node
        third_int = third_int // 10

        while third_int:
            node = ListNode(third_int % 10)
            if prev_node:
                prev_node.next = node
            third_int = third_int // 10
            prev_node = node

        return first_node
